import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';

import { VideoDataService } from '../video-data.service';

import { Video } from '../types';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  videoData$: Observable<Video[]>;

  constructor(private svc: VideoDataService) { }

  ngOnInit(): void {
    this.videoData$ = this.svc.loadVideos();
  }
}
