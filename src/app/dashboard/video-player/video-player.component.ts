import { Component, OnInit, Input } from '@angular/core';

import { VideoDataService } from '../../video-data.service';

import { Video } from '../../types';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.scss']
})
export class VideoPlayerComponent implements OnInit {

  constructor(public svc: VideoDataService) { }

  ngOnInit(): void {
  }

}
