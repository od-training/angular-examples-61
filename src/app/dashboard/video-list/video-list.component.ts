import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { VideoDataService } from '../../video-data.service';

import { Video } from '../../types';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.scss']
})
export class VideoListComponent {
  @Input() videos: Video[];

  constructor(public svc: VideoDataService) { }
}
