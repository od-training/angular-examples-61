import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { tap, map } from 'rxjs/operators';

import { Video } from './types';

function embiggenAuthorNames(videos: Video[]) {
  return videos.map( (item: Video) => ({ ...item, author: item.author.toUpperCase() }) );
}

@Injectable({
  providedIn: 'root'
})
export class VideoDataService {

  currentVideo = new BehaviorSubject<Video>(null);

  constructor(private http: HttpClient) { }

  loadVideos(): Observable<Video[]> {
    return this.http.get<Video[]>('http://api.angularbootcamp.com/videos')
      .pipe(
        map( embiggenAuthorNames ),
        tap( (videos: Video[]) => {
          if (videos && videos[0]) {
            this.currentVideo.next(videos[0]);
          }
        })
      );
  }
}
